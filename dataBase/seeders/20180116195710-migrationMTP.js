const MongoClient = require('mongodb').MongoClient;
const async = require('../../server/utils/promisfied-async');
const _ = require('lodash');
const models = require('../../dataBase/models');

module.exports = {
	up: async (queryInterface, Sequelize) => {
		try {
			const client = await MongoClient.connect('mongodb://localhost:27017/rozetka');
			const db = client.db('rozetka');

			const usersCollection = await db.collection('users');
			const goodsCollection = await db.collection('goods');
			// const infoCollection = await db.collection('info');

			// _.each([1, 2, 3],function (item) {
			// 	console.log(item)
			// });
			//
			await async.each(
				await usersCollection.find().toArray(),//[]

				async function (userFromRozetka) {

					const currentDate = new Date();
					const user = {
						firstName: userFromRozetka.firstName,
						lastName: userFromRozetka.lastName,
						email: userFromRozetka.email,
						password: userFromRozetka.password,
						avatar: userFromRozetka.avatar,
						// idPurchasedGood: userFromRozetka.idPurchasedGood,
						createdAt: currentDate,
						updatedAt: currentDate
					};

					const createdUser = await models.User.create(user);
					// const userRoles = await async.map(
					// 	roles,
					// 	async (role) => _.assign(role, {userId: createdUser.id})
					// );
					// return models.Role.bulkCreate(userRoles);

					return async.each(
						await goodsCollection.find({idPurchasedGood: userFromRozetka._id}).toArray(),
						async (goodsFromRozetka) => models.Good.create(_.assign(goodsFromRozetka/*, {idPurchasedGood: createdUser.id}*/))
					)
				}
			);
			await async.each(
				await goodsCollection.find().toArray(),
				async function (goodFromRozetka) {
					const currentDate = new Date();
					const good = {
						name: goodFromRozetka.name,
						briefCharacteristics: goodFromRozetka.briefCharacteristics,
						fullCharacteristics: goodFromRozetka.fullCharacteristics,
						price: goodFromRozetka.price,
						// category: goodFromRozetka.category,
						picture: goodFromRozetka.picture,
						createdAt: currentDate,
						updatedAt: currentDate
					};
					const createdGood = await models.Good.create(good);
					return async.each(
						goodFromRozetka.info,
						async (info) => models.Info.create(_.assign(info, {goodId: createdGood.id}))
					)
				}
			)

		} catch (ex) {
			console.error(ex);
		}
	},

	down: (queryInterface, Sequelize) => {
		/*
			Add reverting commands here.
			Return a promise to correctly handle asynchronicity.

			Example:
			return queryInterface.bulkDelete('Person', null, {});
		*/
	}
};
