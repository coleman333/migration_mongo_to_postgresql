module.exports = (sequelize, DataTypes) => {
	var Goods = sequelize.define('Good', {
		name: DataTypes.STRING,
		briefCharacteristics: DataTypes.TEXT,
		fullCharacteristics: DataTypes.TEXT,
		price: DataTypes.INTEGER,
		category: DataTypes.INTEGER,

		picture: DataTypes.STRING,
	}, {
		classMethods: {
			associate: function (models) {
				Goods.hasMany(models.Info, {foreignKey: 'goodId'});
				Goods.hasOne(models.category,{foreignKey:'categoryId'});
			}
		}
	});
	return Goods;
};