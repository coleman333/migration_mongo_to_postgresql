'use strict';
module.exports = (sequelize, DataTypes) => {
	var Info = sequelize.define('Info', {
		fieldName: DataTypes.STRING,
		value: DataTypes.TEXT,
		goodId: DataTypes.INTEGER
	}, {
		classMethods: {
			associate: function (models) {
				// associations can be defined here
			}
		}
	});
	return Info;
};